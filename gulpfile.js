var config = require('./config/development.json');
var gulp = require('gulp');
var browserify = require('gulp-browserify');
var watch = require('gulp-watch');
var clean = require('gulp-clean');
var react = require('gulp-react');
var nodemon = require('nodemon');
var sass = require('gulp-sass');

var jsxFiles = config.jsSrc + '/*/**.jsx';
var jsFiles = config.jsSrc + '/*/**.js';

gulp.task('sass', function () {
  gulp.src('public/css/*.scss')
  .pipe(sass())
  .pipe(gulp.dest('./dist'));
});

gulp.task('react', function() {
  gulp.src(jsxFiles)
  .pipe(react())
  .pipe(gulp.dest(config.dist));
});

gulp.task('clean', function (){
  gulp.src(config.dist, {read: false})
    .pipe(clean());
});

gulp.task('browserify', function(){
  gulp.src(config.jsSrc + '/app.js')
    .pipe(browserify({
      insertGlobal: true,
    }))
    .pipe(gulp.dest(config.dist));
});

gulp.task('nodemon', function(){
  nodemon({
    script: 'server.js',
    ext: 'js html',
    // currently not working :/
    ignore: ['public/**', 'gulpfile.js']
  });
});

gulp.task('watch', function() {
  gulp.watch(['public/js/**/*.js'], ['browserify']);
  gulp.watch(['public/css/**/*.scss'], ['sass']);
});

gulp.task('default', ['clean', 'browserify', 'sass', 'nodemon', 'watch']);
