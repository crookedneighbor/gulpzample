var $ = require('jquery');
var Chat = require('./chat');
var Client = require('./models/client');
var Backbone = require('backbone');
var MessageForm = require('./views/message-form');
var MessageBox = require('./views/message-box');
var io = require('socket.io-client');

// provide a wrapper for socket
// that proxies socket events through backbone
// that way views only need to be aware of backbone
// and not the socket itself

module.exports = function () {
  var socket = io.connect();
  var client = new Client();
  var chatApp = new Chat(socket, client);
  var form = new MessageForm({el: '#send-form'});
  var messageBox = new MessageBox({el: '#messages'});

  Backbone.on('messages.outgoing', function (message) {
    console.log('Bus: sending message');
    chatApp.sendMessage(message);
  });

  socket.on('nameResult', function (result) {
    Backbone.trigger('messages.nameResult', result);
  });

  socket.on('message', function (message) {
    console.log('Bus: message incoming: ', message.text);
    Backbone.trigger('messages.incoming', message);
  });

  socket.on('rooms', function(rooms) {
    Backbone.trigger('rooms.roomlist', rooms);
  });

  socket.on('joinResult', function(result) {
    Backbone.trigger('room.join', result);
  });

  setInterval(function() {
    socket.emit('rooms');
  }, 1000);
};
