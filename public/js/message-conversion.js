var $ = require('jquery');

module.exports = {
  checkImg: checkImg
};

function checkImg(msg) {
    if(/http.+\.(gif|png|jpg|jpeg)/i.test(msg)) {
      var start = msg.indexOf("http");
      var end = -1;
      if(msg.indexOf(".jpg") > -1) {
        end = msg.indexOf(".jpg");
      } else if (msg.indexOf(".jpeg") > -1) {
        end = msg.indexOf(".jpeg") + 1;
      } else if (msg.indexOf(".png") > -1) {
        end = msg.indexOf(".png");
      } else if (msg.indexOf(".gif") > -1) {
        end = msg.indexOf(".png");
      }

      var img = msg.substring(start, end + 4);
      msg = msg + "<br><img src='" + img + "'>";
    }
  return msg;

}
