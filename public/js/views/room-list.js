var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.View.extend({
  initialize: function () {
    this.listenTo(Backbone, 'rooms.roomlist', this.handleRoomList);
    this.listenTo(Backbone, 'rooms.currentroom', this.handleCurrent);
    this.listenTo(Backbone, 'rooms.change', this.handleRoomChange);
  },
  events: {
    'click #room-list div': 'joinAction'
  },
  handleCurrent: function () {
    Backbone.trigger('');
  },
  joinAction: function () {
    chatApp.processCommand('/join ' + $(this).text());
    Backbone.trigger('command.join', $(this).text());
    $('#send-message').focus();
  },
  handleRoomChange: function (result) {
      $('#room').text(result.room);
      $('#messages').append(ui.divSystemContentElement('Room changed.'));
      $('#room-list').append(ui.divEscapedContentElement(room));
  },
  handleRoomList: function (e) {
    $('#room-list').empty();
    for(var room in rooms) {
      room = room.substring(1, room.length);
      // what is going on here?
      if (room !== '') {

      }
    }
    form.focus();
  }
});
