var $ = require('jquery');
var Backbone = require('backbone');
Backbone.$ = $;

module.exports = Backbone.View.extend({
  initialize: function () {
    this.$input = this.$('#message');
  },
  events: {
    'keyup input': 'handleKeyup'
  },
  handleKeyup: function(e){
    if (e.keyCode === 13) {
      this.sendInput();
    }
  },
  sendInput: function () {
    var message;

    message = this.$input.val();

    Backbone.trigger('messages.outgoing', message);

    this.$input.val('');
  }
});
