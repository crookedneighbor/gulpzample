var $ = require('jquery');
var Backbone = require('backbone');
var mc = require('../message-conversion');
Backbone.$ = $;

module.exports = Backbone.View.extend({
  initialize: function () {
    this.listenTo(Backbone, 'messages.nameResult', this.handleNameResult);
    this.listenTo(Backbone, 'messages.incoming', this.handleIncomingMessage);
    this.listenTo(Backbone, 'messages.system', this.handleSystemMessage);
    this.listenTo(Backbone, 'room.join', this.handleJoinResult);
  },
  handleNameResult: function (result) {
    console.log('Handling name result');
    var message;
    if (result.success) {
      message = 'You are now known as ' + result.name + '.';
    } else {
      message = result.message;
    }

    this.$el.append(message);
  },
  handleSystemMessage: function (message) {
    console.log('MessageBox: System  message', message);
    var newElement = $('<div class="system"></div>').text(message.text);
    this.$el.append(newElement);
  },
  handleIncomingMessage: function (message) {
    this.addMessage(message.text);
  },
  addMessage: function (text) {
    var msg = text;
    msg = mc.checkImg(msg);
    var newElement = $('<div></div>').html(msg);
    this.$el.append(newElement);
  },
  handleJoinResult: function (result) {
    console.log('Handling join result');
    $('#room').text(result.room);
    this.$el.append('<div>Room changed to ' + result.room + '</div>');
  }
});

