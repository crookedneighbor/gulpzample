var Backbone = require('backbone');

function Chat(socket, chatroom) {
  this.socket = socket;
  this.chatroom = chatroom;
}

Chat.prototype.sendMessage = function (text) {
  if (text.charAt(0) == '/') {
    console.log('system message');
    var systemMessage = this.processCommand(text);
    Backbone.trigger('messages.system', {text: systemMessage});
  } else {
    var message = {
      room: this.chatroom.get('currentRoom'),
      text: text
    };

    console.log('emitting message:', message);
    this.socket.emit('message', message);
  }
};

Chat.prototype.changeRoom = function (room) {
  this.chatroom.set('currentRoom', room);
  this.socket.emit('join', {
    newRoom: room
  });
};

Chat.prototype.processCommand = function (command) {
  var words = command.split(' ');
  command = words[0]
                  .substring(1, words[0].length)
                  .toLowerCase();
  var message = false;

  // TODO: replace with hash
  switch(command) {
    case 'join':
      console.log('processing join');
      words.shift();
      var room = words.join(' ');
      this.changeRoom(room);
      message = "changing rooms";
      break;
    case 'nick':
      words.shift();
      var name = words.join(' ');
      this.socket.emit('nameAttempt', name);
      message = "";
      break;
    default:
      message = 'Unrecognized command.';
      break;
  }

  return message;
};

module.exports = Chat;
