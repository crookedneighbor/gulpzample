This is an unfinished version of an example in chapter 2 of [Node.js in action](http://www.manning.com/cantelon/).

All code is licensed to the original authors.

# Run
```bash
npm install -g gulp
$ gulp
```
